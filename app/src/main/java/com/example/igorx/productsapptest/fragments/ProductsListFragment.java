package com.example.igorx.productsapptest.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.igorx.productsapptest.MyApplication;
import com.example.igorx.productsapptest.R;
import com.example.igorx.productsapptest.adapters.ProductAdapter;
import com.example.igorx.productsapptest.model.ProductModel;
import com.example.igorx.productsapptest.util.AppConst;
import com.example.igorx.productsapptest.util.Preferences;
import com.squareup.okhttp.ResponseBody;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class ProductsListFragment extends Fragment {

   public interface ProductsActionListener{
        void openProduct(ProductModel product);
    }
    private ProductsActionListener productsActionListener;
    private Call<List<ProductModel>> responseCall;
    private ArrayList<ProductModel> products;
    private ProductAdapter productAdapter;
    private ProgressBar progressBar;
    private ListView lvProducts;
    private String accessToken;
    Call<ResponseBody> callComment;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        productsActionListener=(ProductsActionListener)context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.product_list_layout, container,false);
         initView(v);
         products=new ArrayList();
         accessToken= Preferences.getInstance().getAccessToken();
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getProductsFromServer();
    }

    private void initView(View view) {
        progressBar = (ProgressBar) view.findViewById(R.id.pbLoadProd);
        lvProducts = (ListView) view.findViewById(R.id.lvProducts);
        lvProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(productsActionListener!=null ){
                    productsActionListener.openProduct((ProductModel) parent.getItemAtPosition(position));
                }

            }
        });
    }



    private void getProductsFromServer() {
        responseCall = MyApplication.service.getProducts();
        responseCall.enqueue(new Callback<List<ProductModel>>() {
            @Override
            public void onResponse(Response<List<ProductModel>> response, Retrofit retrofit) {
                if(response.isSuccess()) {
                for(ProductModel productModel:response.body()){
                  products.add(productModel);
                }
                    downloadList();
                }

            }

            @Override
            public void onFailure(Throwable t) {
                Log.i(AppConst.TAG, "FAILURE" + t.getMessage());
            }
        });
    }



    private void downloadList() {
        progressBar.setVisibility(View.GONE);
        productAdapter=new ProductAdapter(products);
        lvProducts.setAdapter(productAdapter);
        lvProducts.setVisibility(View.VISIBLE);
        Log.i(AppConst.TAG, "ACCES TOCEN = " + Preferences.getInstance().getAccessToken());
    }


    @Override
    public void onStop() {
        super.onStop();
        if(responseCall!=null){
            responseCall.cancel();
        }
    }
}
