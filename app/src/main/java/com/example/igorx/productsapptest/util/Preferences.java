package com.example.igorx.productsapptest.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.igorx.productsapptest.MyApplication;

public class Preferences {

   private static Preferences instance;

    private SharedPreferences prefs;

    public static Preferences getInstance() {
        if(instance == null) {
            instance = new Preferences();
        }
        return instance;
    }

    public Preferences() {
        initPrefs();
    }

    private void initPrefs() {
        if(null != prefs) return;
        prefs = MyApplication.getInstance().getSharedPreferences(AppConst.NAME_PREFERENCES, 0);
    }

    public void setAccessToken(String accessToken) {
        if(accessToken!=null) {
            prefs.edit().putString(AppConst.NAME_ACCESS_TOKEN, accessToken).apply();
        }
        else clearPreferences();
    }

    public String getAccessToken() {
        return prefs.getString(AppConst.NAME_ACCESS_TOKEN, null);
    }

    public void clearPreferences(){
        prefs.edit().clear().apply();
    }

}
