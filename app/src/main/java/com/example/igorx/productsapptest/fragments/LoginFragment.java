package com.example.igorx.productsapptest.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.igorx.productsapptest.MainActivity;
import com.example.igorx.productsapptest.MyApplication;
import com.example.igorx.productsapptest.R;
import com.example.igorx.productsapptest.model.LoginResponse;
import com.example.igorx.productsapptest.util.AppConst;
import com.example.igorx.productsapptest.util.Preferences;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class LoginFragment extends Fragment implements View.OnClickListener {

    private EditText mUserName, mPassword;
    private Button btnLog, btnSignUp, btnLogGuest;
    private ProductsListFragment prodFragment;
    private RegistrationFragment regFragment;
    private Call<LoginResponse> loginCall;
    private Intent loadProdIntent;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.auth_layout,container,false);
        initView(v);
        return v;
    }

    private void initView(View v) {

        mUserName=(EditText)v.findViewById(R.id.edUserName);
        mPassword=(EditText)v.findViewById(R.id.edPassword);
        btnLog=(Button)v.findViewById(R.id.btnLogin);
        btnSignUp=(Button)v.findViewById(R.id.btnSignUp);
        btnLogGuest=(Button)v.findViewById(R.id.btnLogInAsGuest);
        btnLog.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
        btnLogGuest.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin:
                attemtLogin();
                break;
            case R.id.btnSignUp:
                loadRegistrFragment();
                break;
            case R.id.btnLogInAsGuest:
                loadProductsActivity();
                getActivity().finish();
                break;

        }

    }

    private void attemtLogin() {
        mUserName.setError(null);
        mPassword.setError(null);

       String username=mUserName.getText().toString();
       String password=mPassword.getText().toString();
        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(password)) {
            mPassword.setError(getString(R.string.error_field_required));
            focusView = mPassword;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            mPassword.setError(getString(R.string.error_invalid));
            focusView = mPassword;
            cancel = true;
        }
        if (TextUtils.isEmpty(username)) {
            mUserName.setError(getString(R.string.error_field_required));
            focusView = mUserName;
            cancel = true;
        } else if (!isUsernameValid(username)) {
           mUserName.setError(getString(R.string.error_invalid));
            focusView = mUserName;
            cancel = true;
        }
        if (cancel) {
            focusView.requestFocus();
        } else {
            createLoginRequest();
        }

    }

    private boolean isUsernameValid(String username) {
        return username.length()>3;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 5;
    }

    private void createLoginRequest() {
        loginCall= MyApplication.service.login(mUserName.getText().toString(),mPassword.getText().toString());
        loginCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Response<LoginResponse> response, Retrofit retrofit) {
                if(response.isSuccess() && response.body().getToken()!=null) {
                    Log.i(AppConst.TAG, "Login response = " + response.body().getToken());
                    Preferences.getInstance().setAccessToken(response.body().getToken());
                    loadProductsActivity();
                }
                else {
                    Toast.makeText(getActivity(),"This login / password does not exist",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getActivity(),"Login failure",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadRegistrFragment() {
        regFragment=new RegistrationFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, regFragment);
        ft.commit();
    }
    private void loadProductsActivity() {
        loadProdIntent = new Intent(getActivity(), MainActivity.class);
        startActivity(loadProdIntent);

    }

    @Override
    public void onStop() {
        super.onStop();
        if(loginCall!=null){
            loginCall.cancel();
        }
    }
}
