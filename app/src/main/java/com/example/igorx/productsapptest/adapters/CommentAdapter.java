package com.example.igorx.productsapptest.adapters;


import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.igorx.productsapptest.R;
import com.example.igorx.productsapptest.model.Comment.CommentModel;
import com.example.igorx.productsapptest.util.AppConst;

import java.util.ArrayList;
import java.util.List;

public class CommentAdapter extends BaseAdapter {

    private List<CommentModel> comments=new ArrayList<>();
    private  View mView;

    public CommentAdapter(List<CommentModel> comments){
      this.comments=comments;
    }


    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public Object getItem(int position) {
        return comments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

      ViewHolder holder=null;
        if(convertView==null){
            holder=new ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment_list,null);
            holder.author=(TextView)convertView.findViewById(R.id.author);
            holder.date=(TextView)convertView.findViewById(R.id.date);
            holder.text_comm=(TextView)convertView.findViewById(R.id.text_coment);
            holder.ratingBar=(RatingBar)convertView.findViewById(R.id.ratingBar);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }
        holder.author.setText(comments.get(position).getCreatedBy().getUsername());
        holder.text_comm.setText(comments.get(position).getText());
        holder.date.setText(comments.get(position).getCreatedAt());
        holder.ratingBar.setRating(comments.get(position).getRate());
        return convertView;
    }

    public static class ViewHolder {
        TextView author, date, text_comm;
        RatingBar ratingBar;
    }
}
