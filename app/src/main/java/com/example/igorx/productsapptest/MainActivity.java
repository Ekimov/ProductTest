package com.example.igorx.productsapptest;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.igorx.productsapptest.fragments.CommentDialogFragment;
import com.example.igorx.productsapptest.fragments.DetailsProductFragment;
import com.example.igorx.productsapptest.fragments.LoginFragment;
import com.example.igorx.productsapptest.fragments.ProductsListFragment;
import com.example.igorx.productsapptest.model.ProductModel;
import com.example.igorx.productsapptest.util.AppConst;
import com.example.igorx.productsapptest.util.Preferences;
import com.squareup.okhttp.ResponseBody;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity implements ProductsListFragment.ProductsActionListener,
                                                   CommentDialogFragment.CommentListener, DetailsProductFragment.FabListener {

    private FloatingActionButton fab;
    private DetailsProductFragment detFragment;
    private LoginFragment logFragment;
    private ProductsListFragment prodFragment;
    private Call<ResponseBody> callCommentResponse;
    private Intent loginIntent;
    private TextView tvNoInternet;
    private String productReviews = "http://smktesting.herokuapp.com/api/reviews/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        loadProdFragment();

    }


    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tvNoInternet = (TextView) findViewById(R.id.tvConnection);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.hide();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailsProductFragment detailFragment = (DetailsProductFragment) getSupportFragmentManager()
                        .findFragmentByTag(DetailsProductFragment.class.getCanonicalName());
                if (detailFragment != null) {
                    CommentDialogFragment.instance(detailFragment.getProduct()).show(getSupportFragmentManager(), CommentDialogFragment.class.getCanonicalName());
                }
            }

        });
    }

    private void loadLoginActivity() {
        loginIntent = new Intent(this, LoginActivity.class);
        startActivity(loginIntent);
    }

    private void loadProdFragment() {
        prodFragment = new ProductsListFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragContain, prodFragment);
        ft.addToBackStack(ProductsListFragment.class.getCanonicalName());
        ft.commit();
    }


    @Override
    public void openProduct(ProductModel product) {
        detFragment = new DetailsProductFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragContain, DetailsProductFragment.newInstance(product), DetailsProductFragment.class.getCanonicalName());
        //ft.addToBackStack(DetailsProductFragment.class.getCanonicalName());
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.findItem(R.id.action_logout);
        if (isThereAccessToken()) {
            menuItem.setTitle(R.string.log_out);
        } else {
            menuItem.setTitle(R.string.log_in);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                if (isThereAccessToken()) {
                    Preferences.getInstance().setAccessToken(null);
                }
                if (fab.isShown()) {
                    fab.hide();
                }
                loadLoginActivity();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isThereAccessToken() {
        return Preferences.getInstance().getAccessToken() != null;
    }

    @Override
    public void sendComment(ProductModel product, final int rate, final String comment) {
        JSONObject postJSONComment = new JSONObject();
        try {
            postJSONComment.put("rate", rate);
            postJSONComment.put("text", comment);
        } catch (JSONException e) {
            e.printStackTrace();
        }
           new RequestCommentsAsync(product,postJSONComment).execute();

    }

    @Override
    public void showFab() {
        fab.show();
    }

    @Override
    public void hideFab() {
        fab.hide();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (callCommentResponse != null) {
            callCommentResponse.cancel();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (fab.isShown()) {
            fab.hide();
        }
    }

    public class RequestCommentsAsync extends AsyncTask<Void, Void, Void> {

        JSONObject postJsonComment;
        ProductModel productModel;

       public RequestCommentsAsync(ProductModel product, JSONObject postJsonComment){
           this.productModel=product;
           this.postJsonComment=postJsonComment;
       }

        @Override
        protected Void doInBackground(Void... params) {

            try {
               String loadData=getContent(productReviews+productModel.getId(),postJsonComment);
                Log.i(AppConst.TAG,"Loading DATA= "+loadData);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            openProduct(productModel);
        }
    }


    private String getContent(String path,JSONObject postJsonComment) throws IOException {


        BufferedReader reader = null;
        try {
            URL url = new URL(path);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestProperty("Content-Type", "application/json");
            c.setRequestProperty("Authorization", "Token " + Preferences.getInstance().getAccessToken());
            c.setRequestMethod("POST");
            c.setReadTimeout(10000);

            c.setDoInput(true);
            c.setDoOutput(true);

            OutputStream os = c.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os));
            writer.write(postJsonComment.toString());
            writer.flush();
            writer.close();
            os.close();
            c.connect();

            reader = new BufferedReader(new InputStreamReader(c.getInputStream()));

            StringBuilder buf = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                buf.append(line + "\n");
            }
            return (buf.toString());
        }finally {
            if (reader!= null) {
                reader.close();
            }
        }
    }

}




