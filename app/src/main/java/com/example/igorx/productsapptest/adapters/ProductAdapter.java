package com.example.igorx.productsapptest.adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.igorx.productsapptest.R;
import com.example.igorx.productsapptest.model.ProductModel;
import com.example.igorx.productsapptest.util.AppConst;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductAdapter extends BaseAdapter {

    private ArrayList<ProductModel> products;
    public ProductAdapter(ArrayList<ProductModel> products){
        this.products=products;

    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if(convertView==null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product,null);
            holder=new ViewHolder();
            holder.title=(TextView)convertView.findViewById(R.id.tvTitleProduct);
            holder.discription=(TextView)convertView.findViewById(R.id.tvDescription);
            holder.image=(ImageView)convertView.findViewById(R.id.ivProduct);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.title.setText(products.get(position).getTitle());
        holder.discription.setText(products.get(position).getText());
        Picasso.with(parent.getContext())
                .load(AppConst.IMG_URL+products.get(position).getImg())
                .fit()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.image);
        return convertView;
    }

    public class ViewHolder {

     private TextView title,discription;
     private ImageView image;
    }
}
