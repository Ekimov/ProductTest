package com.example.igorx.productsapptest.model;


import com.example.igorx.productsapptest.model.Comment.CommentModel;
import com.example.igorx.productsapptest.model.Comment.CommentResponse;
import com.squareup.okhttp.ResponseBody;

import java.util.List;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Path;

public interface API {
    ///get all product
    @GET("/api/products/")
    Call<List<ProductModel>> getProducts();

    @GET("/api/reviews/{product_id}")
    Call<List<CommentModel>> getComments(@Path("product_id") long id);

    @FormUrlEncoded
    @POST("/api/register/")
    Call<SignResponse> register(@Field("username") String username,
                                @Field("password") String password);
    @FormUrlEncoded
    @POST("/api/login/")
    Call<LoginResponse> login(@Field("username") String username,
                             @Field("password")String password);
    @FormUrlEncoded
    @POST("/api/reviews/{product_id}")
    Call<ResponseBody> comment(@Path("product_id") long product_id,
                               @Header("Authorization") String token,
                               @Header("Content-Type") String contentType,
                               @Field("rate") int rate,
                               @Field("text") String text);
}
