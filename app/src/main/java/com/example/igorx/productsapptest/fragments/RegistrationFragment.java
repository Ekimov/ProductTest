package com.example.igorx.productsapptest.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.igorx.productsapptest.MainActivity;
import com.example.igorx.productsapptest.MyApplication;
import com.example.igorx.productsapptest.R;
import com.example.igorx.productsapptest.model.SignResponse;
import com.example.igorx.productsapptest.util.AppConst;
import com.example.igorx.productsapptest.util.Preferences;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class RegistrationFragment extends Fragment {

    private EditText regUsername, regPass, regConfPass;
    private Button  btnRegistr;
    private CheckBox chbRemember;
    Call<SignResponse> registerCall;
    private Intent loadProdIntent;
    boolean isValid;
    private ProductsListFragment prodFragment;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.regist_layout,container,false);
        initView(v);
        return v;
    }

    private void initView(View view) {
        btnRegistr=(Button)view.findViewById(R.id.btnRegistr);
        regUsername=(EditText)view.findViewById(R.id.regUserName);
        regPass=(EditText)view.findViewById(R.id.regPassword);
        regConfPass=(EditText)view.findViewById(R.id.regPasswordConfirm);
        chbRemember=(CheckBox)view.findViewById(R.id.chbRegistrRemember);
        btnRegistr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptRegistration();
            }
        });
    }

    private void attemptRegistration() {
        regUsername.setError(null);
        regConfPass.setError(null);
        regPass.setError(null);

        String username=regUsername.getText().toString();
        String password=regPass.getText().toString();
        String passwordConf=regConfPass.getText().toString();
        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(password)) {
            regPass.setError(getString(R.string.error_field_required));
            focusView = regPass;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            regPass.setError(getString(R.string.error_invalid));
            focusView = regPass;
            cancel = true;
        }
        if (TextUtils.isEmpty(username)) {
            regUsername.setError(getString(R.string.error_field_required));
            focusView = regUsername;
            cancel = true;
        } else if (!isUsernameValid(username)) {
            regUsername.setError(getString(R.string.error_invalid));
            focusView = regUsername;
            cancel = true;
        }
        if (TextUtils.isEmpty(passwordConf)) {
            regConfPass.setError(getString(R.string.error_field_required));
            focusView = regConfPass;
            cancel = true;
        } else if (!isPasswordValid(passwordConf)||!isNewPasswordValid(password,passwordConf)) {
            regConfPass.setError(getString(R.string.error_incorect));
            focusView = regConfPass;
            cancel = true;
        }
        if (cancel) {

            focusView.requestFocus();
        } else {
          getRegistrationCall();
        }

    }

    private boolean isNewPasswordValid(String password, String passwordConf) {
        return password.equals(passwordConf);
    }

    private boolean isUsernameValid(String username) {
        return username.length()>3;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 5;
    }


    private void getRegistrationCall() {
        registerCall = MyApplication.service.register(regUsername.getText().toString(),regPass.getText().toString());
        registerCall.enqueue(new Callback<SignResponse>() {
            @Override
            public void onResponse(Response<SignResponse> response, Retrofit retrofit) {
                if (response.isSuccess() && response.body().getToken() != null) {
                    Log.i(AppConst.TAG, "Success Response " + response.body().getToken());
                        Preferences.getInstance().setAccessToken(response.body().getToken());
                        loadProductsActivity();
                }

            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
    private void loadProductsActivity() {
        loadProdIntent = new Intent(getActivity(), MainActivity.class);
        startActivity(loadProdIntent);

    }
    @Override
    public void onStop() {
        super.onStop();
        if(registerCall!=null){
            registerCall.cancel();
        }
    }
}
