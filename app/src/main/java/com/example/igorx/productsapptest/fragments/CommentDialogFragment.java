package com.example.igorx.productsapptest.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import com.example.igorx.productsapptest.R;
import com.example.igorx.productsapptest.model.ProductModel;

public class CommentDialogFragment extends DialogFragment {


    public interface CommentListener{
    void sendComment(ProductModel productModel, int rate, String comment);
 }
    private static final String EXTRA_PRODUCT = "EXTRA_PRODUCT";
   private CommentListener commListner;
    private RatingBar ratingBar;
    private Button sendButton;
    private EditText edComment;
    private ProductModel product;

    public static CommentDialogFragment instance(ProductModel productModel){
        CommentDialogFragment fragment=new CommentDialogFragment();
        Bundle bundle= new Bundle();
        bundle.putSerializable(EXTRA_PRODUCT,productModel);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        commListner = (CommentListener) activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        product=(ProductModel)getArguments().getSerializable(EXTRA_PRODUCT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.comment_dialog,container,false);
        initView(v);
        return v;
    }

    private void initView(View v) {
        edComment=(EditText)v.findViewById(R.id.coment);
        sendButton= (Button) v.findViewById(R.id.sendButton);
        ratingBar= (RatingBar)v.findViewById(R.id.rBar);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(commListner!=null){
                    commListner.sendComment(product,(int)ratingBar.getRating(),edComment.getText().toString());
                    dismiss();
                }
            }
        });
    }
}
