package com.example.igorx.productsapptest.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.igorx.productsapptest.MyApplication;
import com.example.igorx.productsapptest.R;
import com.example.igorx.productsapptest.adapters.CommentAdapter;
import com.example.igorx.productsapptest.model.Comment.CommentModel;
import com.example.igorx.productsapptest.model.ProductModel;
import com.example.igorx.productsapptest.util.AppConst;
import com.example.igorx.productsapptest.util.Preferences;
import com.squareup.okhttp.ResponseBody;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class DetailsProductFragment extends Fragment {

    public interface FabListener{
        void showFab();
        void hideFab();
    }
    private FabListener fabListener;
    private static final String EXTRA_PRODUCT = "EXTRA_PRODUCT";
    private ImageView detailsImage;
    private TextView detailsTitle, detailsDescription;
    private ListView commList;
    private RelativeLayout progressLayout;
    private Call<List<CommentModel>> callComment;
    private List<CommentModel> listComment;
    public ProductModel getProduct() {
        return product;
    }

    private ProductModel product;
    private CommentAdapter commentAdapter;
    private String accessToken;

    public static Fragment newInstance(ProductModel product) {
        Fragment fragment = new DetailsProductFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_PRODUCT, product);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       fabListener=(FabListener)context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        product = (ProductModel) getArguments().getSerializable(EXTRA_PRODUCT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.detail_product, container, false);
        initView(v);

        accessToken = Preferences.getInstance().getAccessToken();

        return v;
    }

    private void initView(View v) {
        detailsDescription = (TextView) v.findViewById(R.id.tvDetailsDescription);
        detailsTitle = (TextView) v.findViewById(R.id.tvDetailsTitle);
        detailsImage = (ImageView) v.findViewById(R.id.imDetails);
        commList = (ListView) v.findViewById(R.id.lvComments);
        progressLayout=(RelativeLayout)v.findViewById(R.id.progressLayout);
        detailsDescription.setText(product.getText());
        detailsTitle.setText(product.getTitle());
        Picasso.with(v.getContext())
                .load(AppConst.IMG_URL + product.getImg())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(detailsImage);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getCommentsProduct(product);
        if(accessToken!=null) {
            fabListener.showFab();
        }
    }

    void getCommentsProduct (ProductModel product) {
       callComment=  MyApplication.service.getComments(product.getId());
        callComment.enqueue(new Callback<List<CommentModel>>() {

            @Override
            public void onResponse(Response<List<CommentModel>> response, Retrofit retrofit) {
                if (response.isSuccess()) {

                    loadListComment(response.body());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.i(AppConst.TAG, "Details OnFailure " + t.getMessage());
            }
        });
    }

    void loadListComment( List<CommentModel> listComment){
        progressLayout.setVisibility(View.GONE);
        commList.setVisibility(View.VISIBLE);
        commentAdapter = new CommentAdapter(listComment);
        commList.setAdapter(commentAdapter);
    }


   public void addNewComment(CommentModel comment){
              listComment.add(comment);
              commentAdapter.notifyDataSetChanged();

   }
    @Override
    public void onStop() {
        super.onStop();
        if(callComment!=null){
            callComment.cancel();
        }
    }
}
