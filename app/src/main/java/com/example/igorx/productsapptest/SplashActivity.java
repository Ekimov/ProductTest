package com.example.igorx.productsapptest;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.example.igorx.productsapptest.util.AppConst;
import com.example.igorx.productsapptest.util.Preferences;

public class SplashActivity extends AppCompatActivity {

    private LinearLayout splashLogo;
    private Animation anim;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        splashLogo=(LinearLayout)findViewById(R.id.splashLogo);
        anim = AnimationUtils.loadAnimation(getBaseContext(), R.anim.splash_anim);
        splashLogo.setAnimation(anim);
        timeSplashScreen();
    }

    private void timeSplashScreen(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startProducts();
            }
        }, 1 * 1800);
    }

    private void startProducts() {
        Intent intent;
        if(Preferences.getInstance().getAccessToken()!=null){
            Log.i(AppConst.TAG,"start MainActivity");
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);

        }
        else {
            Log.i(AppConst.TAG,"start LoginActivity");
            intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
        finish();
    }
}
