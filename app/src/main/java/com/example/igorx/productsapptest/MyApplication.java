package com.example.igorx.productsapptest;

import android.app.Application;

import com.example.igorx.productsapptest.model.API;
import com.example.igorx.productsapptest.util.AppConst;
import com.squareup.okhttp.OkHttpClient;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;


public class MyApplication extends Application {

    private  static MyApplication singlton;
    public static API service;

    public  static MyApplication getInstance(){
        return singlton;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        singlton = this;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConst.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service=retrofit.create(API.class);

    }
}



